
Prerequisites
-------------

- nodejs v4.4.2
- gulp and gulp-cli
- typings
- typescrit
- ts-node

Running
-------

Install dependencies:

> npm install

`node_modules` and `typings` directories should be created during the install.

Install bower components:

> bower install

`bower_components` directory should be created during the install.

Build the project:

> npm run clean & npm run build

`build` directory should be created during the build

> npm start

The application should be displayed in the browser.
