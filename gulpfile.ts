"use strict";

const gulp = require("gulp");
const del = require("del");
const tsc = require("gulp-typescript");
const sourcemaps = require('gulp-sourcemaps');
const tsProject = tsc.createProject("tsconfig.json");
const less = require('gulp-less');
const LessAutoprefix = require('less-plugin-autoprefix');
const autoprefix = new LessAutoprefix({browsers: ['last 2 versions']});
const concat = require('gulp-concat');
const pug = require('gulp-pug');
const tslint = require('gulp-tslint');

/**
 * Remove build directory.
 */
gulp.task('clean', (cb) => {
    return del(["build"], cb);
});

/**
 * Lint all custom TypeScript files.
 */
gulp.task('tslint', () => {
    return gulp.src("src/app/**/*.ts")
        .pipe(tslint())
        .pipe(tslint.report('prose'));
});

/**
 * Compile TypeScript sources and create sourcemaps in build directory.
 */
gulp.task("compile", ["tslint"], () => {
    let tsResult = gulp.src("src/app/**/*.ts")
        .pipe(sourcemaps.init())
        .pipe(tsc(tsProject));
    return tsResult.js
        .pipe(sourcemaps.write("."))
        .pipe(gulp.dest("build/"));
});

/**
 * Compile Styles sources and create sourcemaps in build directory.
 */
gulp.task("styles", () => {
    return gulp.src(["src/styles/inc/**/*.less", "src/styles/modules/**/*.less"])
        .pipe(sourcemaps.init())
        .pipe(concat('styles.less'))
        .pipe(less({
            plugins: [autoprefix]
        }))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest("build/"));
});

gulp.task("theme", () => {
    return gulp.src(["bower_components/bootstrap/less/bootstrap.less", "src/styles/theme/variables.less"])
        .pipe(sourcemaps.init())
        .pipe(concat('theme.less'))
        .pipe(less())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest("build/"));
});

/**
 * Compile Jade sources and create in build directory.
 */
gulp.task("templates", () => {
    return gulp.src("src/app/**/*.jade")
        .pipe(pug({
            pretty: true
        }))
        .pipe(gulp.dest("build/"));
});

/**
 * Copy all images files into build directory.
 */
gulp.task("images", () => {
    return gulp.src("src/images/**/*.*")
        .pipe(gulp.dest("build/images/"));
});

/**
 * Copy all JavaScript files into build directory.
 */
gulp.task("resource", () => {
    return gulp.src(["src/**/*.js", "src/app/**/*.json"])
        .pipe(gulp.dest("build/"));
});

/**
 * Copy all required libraries into build directory.
 */
gulp.task("libs", () => {
    return gulp.src([
        'es6-shim/es6-shim.min.js',
        'systemjs/dist/system-polyfills.js',
        'systemjs/dist/system.src.js',
        'reflect-metadata/Reflect.js',
        'rxjs/**',
        'angular2-in-memory-web-api/**',
        'ng2-bootstrap/**',
        'moment/**',
        'zone.js/dist/**',
        '@angular/**',
        'font-awesome/**'
    ], {cwd: "node_modules/**"}) /* Glob required here. */
        .pipe(gulp.dest("build/lib"));
});

/**
 * Watch for changes in TypeScript, Jade and LESS files.
 */
gulp.task('watch', function () {
    gulp.watch(["src/**/*.ts"], ['compile']).on('change', function (e) {
        console.log('TypeScript file ' + e.path + ' has been changed. Compiling.');
    });
    gulp.watch(["src/**/*.jade"], ['templates']).on('change', function (e) {
        console.log('Template file ' + e.path + ' has been changed. Updating.');
    });
    gulp.watch(["src/styles/**/*.less"], ['styles']).on('change', function (e) {
        console.log('Style file ' + e.path + ' has been changed. Updating.');
    });
    gulp.watch(["src/app/**/*.json"], ['resource']).on('change', function (e) {
        console.log('Style file ' + e.path + ' has been changed. Updating.');
    });
});

/**
 * Build the project.
 */
gulp.task("default", ['compile', 'templates', 'styles'], () => {
    console.log("Building the not minifyed project ...");
});

gulp.task("build", ['compile', 'resource', 'templates', 'theme', 'styles', 'images', 'libs'], () => {
    console.log("Building the not minifyed project ...");
});