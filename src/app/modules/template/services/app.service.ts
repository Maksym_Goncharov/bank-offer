import {Injectable} from "@angular/core";

@Injectable()

export class AppService {

    getMenu() {
        return ['home', 'races', 'live streams', 'promotions', 'statistics', 'help'];
    }
}