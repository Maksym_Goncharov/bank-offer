import {Component} from "@angular/core";

import {HeaderComponent} from "./header.component";

@Component({
    selector: "app",
    templateUrl: "./modules/template/views/app.html",
    directives: [HeaderComponent],
})

export class AppComponent {
}